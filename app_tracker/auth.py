import re

from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask_login import login_user, login_required, logout_user
from werkzeug.security import generate_password_hash, check_password_hash

from app_tracker import db
from app_tracker.models import User

auth = Blueprint('auth', __name__)


@auth.route('/login', methods=['POST', 'GET'])
def login(email=''):
    if request.method == 'POST':
        input_email = request.form.get('email')
        password = request.form.get('password')
        remember = True if request.form.get('remember') else False

        user = User.query.filter_by(email=input_email).first()

        if not user or not check_password_hash(user.password, password):
            flash('Please check your login details and try again.')
            return render_template('login.html', email=input_email)

        login_user(user, remember=remember)
        return redirect(url_for('main.profile'))

    return render_template('login.html', email=email)


@auth.route('/signup', methods=['POST', 'GET'])
def signup(email='', name=''):
    if request.method == 'POST':
        input_email = request.form.get('email')
        input_name = request.form.get('name')
        password = request.form.get('password')
        gender = request.form.get('gender')

        user = User.query.filter_by(email=input_email).first()
        if user:
            flash('Email address already exists')
            return redirect(url_for('auth.signup'))

        if len(password) <= 5 or not re.search(r'[0-9]', password):
            flash('Password length should be more than 5 and it must contain at least 1 number')
            return render_template('signup.html', email=input_email, name=input_name)

        if not re.findall(r"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", input_email):
            flash('Invalid email')
            return render_template('signup.html', email=input_email, name=input_name)

        new_user = User(email=input_email, name=input_name,
                        password=generate_password_hash(password, method='sha256'),
                        gender=gender)

        db.session.add(new_user)
        db.session.commit()

        return redirect(url_for('auth.login'))

    return render_template('signup.html', email=email, name=name)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))