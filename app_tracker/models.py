from datetime import datetime

from flask_login import UserMixin

from app_tracker import db


class User(UserMixin, db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(), unique=True, nullable=False)
    password = db.Column(db.String(), nullable=False)
    name = db.Column(db.String(), nullable=False)
    gender = db.Column(db.String(), nullable=True)
    picture = db.Column(db.String(256), default=None)

    def __init__(self, email, password, name, gender):
        self.email = email
        self.password = password
        self.name = name
        self.gender = gender


class Content(db.Model):
    __tablename__ = 'content'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String())
    type = db.Column(db.String())
    genres = db.Column(db.ARRAY(db.String))
    picture = db.Column(db.String(256), default=None)


class ContentList(db.Model):
    __tablename__ = 'content_list'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'), nullable=False)
    content_id = db.Column(db.Integer, db.ForeignKey('content.id', ondelete='CASCADE'), nullable=False)
    status = db.Column(db.String(), nullable=False)
    mark = db.Column(db.Integer, nullable=True)
    comment = db.Column(db.String(), nullable=True)
    date_added = db.Column(db.DateTime(timezone=True), default=datetime.utcnow())

