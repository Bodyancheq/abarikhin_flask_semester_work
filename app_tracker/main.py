import json
import os
import re

from flask import Blueprint, render_template, request, redirect, flash, url_for
from flask_login import login_required, current_user
from werkzeug.security import generate_password_hash

from app_tracker import db
from app_tracker.models import User, ContentList
from app_tracker.services import get_anime_list, get_manga_list, get_films_list, get_books_list, \
    get_content_by_title, get_user_anime_list, get_user_manga_list, get_user_films_list, \
    get_user_books_list, get_title_info, get_content_comments

main = Blueprint('main', __name__)
UPLOAD_FILES_DIR = 'uploads'


@main.route('/', methods = ['GET', 'POST'])
@main.route('/<int:page>', methods=["GET", "POST"])
def index(page=1):
    if request.method == 'POST':
        title = get_content_by_title(request.form.get('search_query'))
        if title:
            return redirect(f'/title/{title[0][0]}')
        else:
            flash('Title not found')


    anime_list = get_anime_list(page)
    manga_list = get_manga_list(page)
    film_list = get_films_list(page)
    book_list = get_books_list(page)
    return render_template('index.html',
                           anime_list=anime_list,
                           manga_list=manga_list,
                           film_list=film_list,
                           book_list=book_list)


@main.route('/profile')
@login_required
def profile():
    anime_list = get_user_anime_list(current_user.id)
    manga_list = get_user_manga_list(current_user.id)
    film_list = get_user_films_list(current_user.id)
    book_list = get_user_books_list(current_user.id)
    user = User.query.filter_by(email=current_user.email).first()
    return render_template('profile.html',
                           name=current_user.name,
                           user=user,
                           anime_list=anime_list,
                           manga_list=manga_list,
                           film_list=film_list,
                           book_list=book_list)


@main.route('/profile_settings', methods=['POST', 'GET'])
@login_required
def profile_settings():
    if request.method == 'POST':
        name = request.form.get('name')
        password = request.form.get('password')
        file = request.files.get('photo')
        user = User.query.filter_by(email=current_user.email).first()
        if file:
            file_ext = file.filename.rsplit('.', 1)[1]
            file_path = os.path.join(
                UPLOAD_FILES_DIR,
                f"{generate_password_hash(file.filename).replace(':', '')}.{file_ext}"
            )
            file.save(os.path.join("app_tracker", "static", file_path))
            user.picture = file_path
            db.session.commit()
            flash('Picture updated')
        if name:
            user.name = name
            db.session.commit()
            flash('Name changed')
        if password and not (len(password) <= 5 or not re.search(r'[0-9]', password)):
            user.password = generate_password_hash(password, method='sha256')
            db.session.commit()
            flash('Password changed')
        elif password:
            flash('Password length should be more than 5 and it must contain at least 1 number')
    return render_template('profile_settings.html')


# MAY BE IMPLEMENTED LATER
# @main.route('/anime_list')
# @login_required
# def anime_list():
#     return render_template('anime_list.html', name=current_user.name)
#
#
# @main.route('/manga_list')
# @login_required
# def manga_list():
#     return render_template('manga_list.html', name=current_user.name)
#
#
# @main.route('/film_list')
# @login_required
# def film_list():
#     return render_template('film_list.html', name=current_user.name)
#
#
# @main.route('/book_list')
# @login_required
# def book_list():
#     return render_template('book_list.html', name=current_user.name)


@main.route('/title/<int:title_id>', methods=['POST', 'GET'])
@login_required
def title(title_id):
    title = get_title_info(title_id)
    if request.method == 'POST':
        content_record = ContentList.query.filter_by(user_id=current_user.id,
                                                     content_id=title_id).first()
        if content_record:
            usr_comment = request.form.get('comment')
            content_record.comment = usr_comment
            db.session.commit()
            flash('Comment added successfully')
            return redirect(url_for('main.title', title_id=title_id))

    if not title:
        flash('No such title')
        redirect(url_for('main.index'))
    else:
        record = ContentList.query.filter_by(user_id=current_user.id,
                                             content_id=title_id).first()
        try:
            comments = list(map(lambda x: x[0].strip(), get_content_comments(title_id)))
        except AttributeError:
            comments = []

        if record:
            comment = record.comment
        else:
            comment = False
        return render_template('title.html',
                               id=title_id,
                               name=title.name,
                               type=title.type,
                               genres=list(title.genres),
                               picture=title.picture,
                               user_has_title=True if record else False,
                               user_status=record.status if record else None,
                               rating=record.mark if record else None,
                               comment=comment,
                               comments = comments
                               )


@main.route('/add_title/<int:title_id>', methods=["GET", "POST"])
@login_required
def add_title(title_id):
    if request.method == "POST":
        content_record = ContentList.query.filter_by(user_id=current_user.id,
                                                     content_id=title_id).first()
        if content_record:
            flash('Content already in your list')
            return redirect(url_for('main.title', title_id=title_id))

        list_name = request.form.get('list')
        rating = request.form.get('rating')
        new_record = ContentList(user_id=current_user.id, content_id=title_id, status=list_name,
                                 mark=(rating or None))
        db.session.add(new_record)
        db.session.commit()
        flash('Content added successfully')
        return redirect(url_for('main.title', title_id=title_id))

    title = get_title_info(title_id)
    return render_template('add_title.html',
                           id=title_id,
                           name=title.name)


@main.route('/change_status/<int:title_id>', methods=["GET", "POST"])
@login_required
def change_status(title_id):
    if request.method == "POST":
        content_record = ContentList.query.filter_by(user_id=current_user.id,
                                                     content_id=title_id).first()
        if not content_record:
            flash('Content is not in your list')
            return redirect(url_for('main.title', title_id=title_id))

        list_name = request.form.get('list')
        rating = request.form.get('rating')
        content_record.status = list_name
        content_record.mark = rating
        db.session.commit()
        flash('Entry changed successfully')
        return redirect(url_for('main.title', title_id=title_id))

    title = get_title_info(title_id)
    return render_template('change_status.html',
                           id=title_id,
                           name=title.name)


@main.route('/ajax_rate/<int:title_id>', methods=["POST"])
@login_required
def ajax_rate(title_id):
    if request.method == "POST":
        content_record = ContentList.query.filter_by(user_id=current_user.id,
                                                     content_id=title_id).first()
        if not content_record:
            return json.dumps({'success': 'false', 'msg': 'Ошибка на сервере!'})

        rating = request.form.get('rating', None) or None
        if rating != 'None':
            content_record.mark = rating
            db.session.commit()
            return json.dumps({'success': 'true', 'msg': 'Mark added', 'rating': f'{rating}'})
        else:
            return json.dumps({'success': 'false', 'msg': 'Mark was not added', 'rating': f'{rating}'})
