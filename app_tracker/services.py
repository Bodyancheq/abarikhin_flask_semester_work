from sqlalchemy import text, select
from flask_login import current_user

from app_tracker.models import Content, ContentList
from app_tracker import db

POSTS_PER_PAGE = 10


def get_anime_list(page_num: int = 1):
    animes = Content.query.filter_by(type='Anime').paginate(page_num, POSTS_PER_PAGE, False)
    return animes


def get_manga_list(page_num: int = 1):
    manga = Content.query.filter_by(type='Manga').paginate(page_num, POSTS_PER_PAGE, False)
    return manga


def get_films_list(page_num: int = 1):
    films = Content.query.filter_by(type='Film').paginate(page_num, POSTS_PER_PAGE, False)
    return films


def get_books_list(page_num: int = 1):
    books = Content.query.filter_by(type='Book').paginate(page_num, POSTS_PER_PAGE, False)
    return books


def get_content_by_title(name: str) -> list or None:
    with db.engine.connect() as connection:
        content = connection.execute(
            text("""
                SELECT * FROM "content" 
                WHERE LOWER(name) LIKE 
                LOWER(:content_name)
                LIMIT 1"""),
            content_name=f"%{name}%"
        ).fetchall()
        if content:
            return content
        else:
            return None


def get_user_anime_list(id_: int) -> list:
    with db.engine.connect() as connection:
        animes = connection.execute(
            text("""
            SELECT content.id,
                   name,
                   type,
                   genres,
                   picture
            FROM content JOIN content_list on content.id = content_list.content_id
            WHERE content_list.user_id = :usr_id
            AND type = 'Anime' """),
            usr_id=str(id_)
        ).fetchall()
        return animes


def get_user_manga_list(id_: int) -> list:
    with db.engine.connect() as connection:
        manga = connection.execute(
            text("""
            SELECT content.id,
                   name,
                   type,
                   genres,
                   picture
            FROM content JOIN content_list on content.id = content_list.content_id
            WHERE content_list.user_id = :usr_id
            AND type = 'Manga' """),
            usr_id=str(id_)
        ).fetchall()
        return manga


def get_user_films_list(id_: int) -> list:
    with db.engine.connect() as connection:
        films = connection.execute(
            text("""
            SELECT content.id,
                   name,
                   type,
                   genres,
                   picture
            FROM content JOIN content_list on content.id = content_list.content_id
            WHERE content_list.user_id = :usr_id
            AND type = 'Film' """),
            usr_id=str(id_)
        ).fetchall()
        return films


def get_user_books_list(id_: int) -> list:
    with db.engine.connect() as connection:
        books = connection.execute(
            text("""
            SELECT content.id,
                   name,
                   type,
                   genres,
                   picture
            FROM content JOIN content_list on content.id = content_list.content_id
            WHERE content_list.user_id = :usr_id
            AND type = 'Book' """),
            usr_id=str(id_)
        ).fetchall()
        return books


def get_title_info(title_id: int) -> Content or None:
    title = Content.query.filter_by(id=title_id).first()
    return title


def get_content_comments(title_id: int) -> list:
    stmt = select(ContentList.comment).where(ContentList.content_id == title_id and
                                             ContentList.comment is not None)
    with db.engine.connect() as connection:
        comments = connection.execute(stmt)
        return comments
