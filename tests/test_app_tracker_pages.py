import random

from app_tracker import create_app

app = create_app()
app.testing = True
email = 'abobus@mail.ru'
password = '123aboba'


def login(client, email, password):
    return client.post('/login', data=dict(
        email=email,
        password=password
    ), follow_redirects=True)


def logout(client):
    return client.get('/logout', follow_redirects=True)


def test_index_is_ok():
    client = app.test_client()
    response = client.get('/')
    assert response.status_code == 200


def test_login():
    client = app.test_client()
    rv = login(client, email, password)
    assert b'Welcome' in rv.data


def test_wrong_login():
    client = app.test_client()
    rv = login(client, "abobus@mail.r", password)
    assert b'Please check your login details and try again.' in rv.data


def test_logout():
    client = app.test_client()
    client.post('/login', data=dict(
        email=email,
        password=password,
        remember=True
    ), follow_redirects=True)
    rv = logout(client)
    assert b'Register' in rv.data


def test_signup():
    client = app.test_client()
    rv = client.post('/signup', data=dict(
        email=f"test{random.randint(1, 1000000)}@mail.ru",
        name="test",
        password="123aboba",
        gender="female"
    ), follow_redirects=True)
    assert b'Login' in rv.data


def test_signup_wrong_email():
    client = app.test_client()
    rv = client.post('/signup', data=dict(
        email=f"test{random.randint(1, 1000000)}@mail",
        name="test",
        password="123aboba",
        gender="female"
    ), follow_redirects=True)
    assert b'Invalid email' in rv.data


def test_signup_wrong_password():
    client = app.test_client()
    rv = client.post('/signup', data=dict(
        email=f"test{random.randint(1, 1000000)}@mail.ru",
        name="test",
        password="aboba",
        gender="female"
    ), follow_redirects=True)
    assert b'Password length should be more than 5 and it must contain at least 1 number' in rv.data


def test_signup_email_already_exists():
    client = app.test_client()
    rv = client.post('/signup', data=dict(
        email=f"abobus@mail.ru",
        name="test",
        password="123aboba",
        gender="female"
    ), follow_redirects=True)
    assert b'Email address already exists' in rv.data


def test_profile_page():
    client = app.test_client()
    client.post('/login', data=dict(
        email=email,
        password=password
    ), follow_redirects=True)

    rv = client.get('/profile')

    assert b'Welcome' in rv.data


def test_profile_settings():
    client = app.test_client()
    client.post('/login', data=dict(
        email=email,
        password=password
    ), follow_redirects=True)

    rv = client.get('/profile_settings')

    assert b'Profile settings' in rv.data


def test_profile_settings():
    client = app.test_client()
    client.post('/login', data=dict(
        email=email,
        password=password
    ), follow_redirects=True)

    client.post('/profile_settings', data=dict(
        name='BOGDAN',
    ), follow_redirects=True)

    rv = client.get('/profile')

    assert 'BOGDAN' in rv.data.decode('UTF-8')


def test_password_change_wrong_password():
    client = app.test_client()
    client.post('/login', data=dict(
        email=email,
        password=password
    ), follow_redirects=True)

    rv = client.post('/profile_settings', data=dict(
        name='BOGDAN',
        password='123'
    ), follow_redirects=True)

    assert 'Password length should be more than 5 and it must contain at least 1 number' in rv.data.decode('UTF-8')


def test_search_for_title():
    client = app.test_client()
    client.post('/login', data=dict(
        email=email,
        password=password
    ), follow_redirects=True)

    rv = client.post('/', data=dict(
        search_query='ДжоДжо',
    ), follow_redirects=True)

    assert 'Невероятное приключение ДжоДжо — Часть 7: Гонка «Стальной шар»' in \
           rv.data.decode('UTF-8')


def test_title_page():
    client = app.test_client()
    client.post('/login', data=dict(
        email=email,
        password=password
    ), follow_redirects=True)

    rv = client.post('/title/19', data=dict(
        comment='КРУТАЯ МАНГА',
    ), follow_redirects=True)

    assert 'КРУТАЯ МАНГА' in rv.data.decode('UTF-8')


def test_add_title_page():
    client = app.test_client()
    client.post('/login', data=dict(
        email=email,
        password=password
    ), follow_redirects=True)

    rv = client.get('/add_title/1', follow_redirects=True)

    assert 'Add title Стальной алхимик: Братство' in rv.data.decode('UTF-8')


def test_add_title_post():
    client = app.test_client()
    client.post('/login', data=dict(
        email=email,
        password=password
    ), follow_redirects=True)

    rv = client.post('/add_title/19', data=dict(
        list='Planned',
        rating=None,
    ), follow_redirects=True)

    assert 'Your rating: ' in rv.data.decode('UTF-8')


def test_change_status_page():
    client = app.test_client()
    client.post('/login', data=dict(
        email=email,
        password=password
    ), follow_redirects=True)

    rv = client.get('/change_status/19', follow_redirects=True)

    assert 'Changing status for Слэм-данк' in rv.data.decode('UTF-8')


def test_change_status_post_query():
    client = app.test_client()
    client.post('/login', data=dict(
        email=email,
        password=password
    ), follow_redirects=True)

    rv = client.post('/change_status/19', data=dict(
        list='Done',
        rating=10,
    ), follow_redirects=True)

    assert 'Entry changed successfully' in rv.data.decode('UTF-8')


def test_ajax_post():
    client = app.test_client()
    client.post('/login', data=dict(
        email=email,
        password=password
    ), follow_redirects=True)

    rv = client.post('/ajax_rate/19', data=dict(
        rating=9
    ), follow_redirects=True)

    assert 'Mark added' in rv.data.decode('UTF-8')