# Abarikhin_flask_semester_work

Service for films, anime, manga and books progress-tracking

# Install
```
# clone the repository
$ git clone https://gitlab.com/Bodyancheq/abarikhin_flask_semester_work.git
```
# Create a virtualenv and activate it:
```
$ python3 -m venv venv
$ . venv/bin/activate`
```
# Or on Windows cmd:
```
`$ py -3 -m venv venv
$ venv\Scripts\activate.bat`
```
# Install pacakges
`$ pip install requirements.txt`

# Database
Setup postgres via PGAdmin4 or docker and enter credentials into `create_app` function, where `SQLALCHEMY_DATABASE_URI` is written
Then run migrations with
```
flask db upgrade
```

# Run
```
$ export FLASK_APP=app_trackrer
$ flask run
```
# Or on Windows cmd:
```
> set FLASK_APP=app_trackrer
> flask run
```
Open http://127.0.0.1:5000 in a browser.

# Test
`$ pytest tests`
